from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'games.views.home', name='home'),
    url(r'^login$', 'games.views.login_page', name='login-page'),
    url(r'^login-view$', 'games.views.login_view', name='login'), 
    url(r'^register$', 'games.views.register', name='register'),
    url(r'^logout-view$', 'games.views.logout_view', name='logout'),
    url(r'^new_submission$', 'games.views.new_submission', name='new_submission'),
    url(r'^([a-z]*)$', 'games.views.game_details', 
        name='game-details'),
    url(r'^([a-z]*)/standings$', 'games.views.game_ratings',
        name='game-ratings'), 
    url(r'^([a-z]*)/round-(\d*)$', 'games.views.round_standings', 
        name='round-standings'),
    url(r'^([a-z]*)/submissions$', 'games.views.game_submissions',
        name='game-submissions'),
    url(r'^([a-z]*)/get_submissions$', 'games.views.submissions_json', 
        name='get_submissions'), 
    url(r'^([a-z]*)/round-(\d*)/matches$', 'games.views.round_matches',
        name='round-matches'),
    url(r'^([a-z]*)/submission-info/(\d*)$', 'games.views.submission_info',
        name='submission-info'),

    # Game admin options 
    url(r'^([a-z]*)/edit$', 'games.views.edit_game_details',
        name='edit-game-details'),
    url(r'^([a-z]*)/save$', 'games.views.save_game_details',
        name='save-game-details'), 
    url(r'^([a-z]*)/view-history$', 'games.views.game_history',
        name='view-game-history'),
 
    url(r'^match-log/$', 'games.views.match_log', name='match-log'),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
