from copy import copy

from eval.exceptions import InvalidMoveException

BOARD_SIZE = 5

class Dots(object):
    def __init__(self, players):
        self.players = players
        self.moves = []

    def get_other_player(self, current_player):
        """ Given the id of the current player returns the id of the other 
        player.
        """
        if current_player == self.players[0]:
            return self.players[1]
        else:
            return self.players[0]

    def get_match_log(self):
        return self.moves

    def get_initial_state(self):
        starting_cell = {
            'top': 0,
            'left': 0,
            'right': 0,
            'bottom': 0
        }
        self.board_owner = []
        board = []
        for i in xrange(0, BOARD_SIZE):
            line = []
            for j in xrange(0, BOARD_SIZE):
                line.append(copy(starting_cell))
            board.append(copy(line))
            self.board_owner.append([0] * BOARD_SIZE)

        return board

    def game_ended(self, game_state):
        for i in xrange(0, BOARD_SIZE):
            for j in xrange(0, BOARD_SIZE):
                for border in game_state[i][j].itervalues():
                    if not border:
                        return False
        return True

    def game_winner(self, game_state):
        """ Compute game_winner """
        if not self.game_ended(game_state):
            # No winner yet
            return None

        scores = {
            self.players[0]: 0,
            self.players[1]: 0
        }

        for i in xrange(0, BOARD_SIZE):
            for j in xrange(0, BOARD_SIZE):
                cell_owner = self.board_owner[i][j]
                scores[cell_owner] += 1

        if scores[self.players[0]] > scores[self.players[1]]:
            return self.players[0]
        elif scores[self.players[0]] < scores[self.players[0]]:
            return self.players[1]
        else:
            return 'TIE'

    def get_neighbour_cell(self, x, y, border):
        new_border = ''
        if border == 'top':
            x = x - 1
            new_border = 'bottom'
        elif border == 'bottom':
            x = x + 1
            new_border = 'top'
        elif border == 'left':
            y = y - 1
            new_border = 'right'
        else:
            y = y + 1
            new_border = 'left'

        if x < 0 or x >= BOARD_SIZE or y < 0 or y >= BOARD_SIZE:
            return None

        return x, y, new_border

    def validate_move(self, game_state, player, move):
        x, y, border = move

        if x < 0 or x >= BOARD_SIZE or y < 0 or y >= BOARD_SIZE:
            raise InvalidMoveException("Cell out of bounds: (%s, %s)" % (x, y)) 
        if border not in ['top', 'right', 'left', 'bottom']:
            raise InvalidMoveException("Invalid border '%s'" % border)
        if game_state[x][y][border]:
            raise InvalidMoveException("Border already colored.")

    def execute_move(self, game_state, player, move):
        self.validate_move(game_state, player, move)

        # Make the move
        x, y, border = move
        game_state[x][y][border] = player
        next_player = self.get_other_player(player)

        # Create a move dict which will be added to the match log
        move_log = {'cell': [x, y], 'border': border, 'captures': [], 'player': player}

        # Make the move in the cell sharing the border
        neighbour_cell = self.get_neighbour_cell(x, y, border)
        if neighbour_cell:
            cell_x, cell_y, cell_border = neighbour_cell
            game_state[cell_x][cell_y][cell_border] = player;
            # Also the neighbouring cell might be complete
            if 0 not in game_state[cell_x][cell_y].values():
                self.board_owner[cell_x][cell_y] = player
                move_log['captures'].append([cell_x, cell_y])
                next_player = player

        # Check if the cell is completely enclosed
        # If it is the current player moves again, otherwise the other player
        # is next
        if 0 not in game_state[x][y].values():
            # cell is complete
            # Mark the player as owner of this cell
            self.board_owner[x][y] = player
            move_log['captures'].append([x, y])
            next_player = player

        self.moves.append(move_log)
        return game_state, next_player
