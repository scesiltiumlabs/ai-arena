from eval.exceptions import InvalidMoveException

class Tictactoe(object):
    def __init__(self, players):
        self.players = players
        self.moves = []

    def get_match_log(self):
        return self.moves

    def get_initial_state(self):
        return [[0, 0, 0], [0, 0, 0], [0, 0, 0]]

    def get_other_player(self, current_player):
        """ Given the id of the current player returns the id of the other 
        player.
        """
        if current_player == self.players[0]:
            return self.players[1]
        else:
            return self.players[0]

    def validate_move(self, game_state, player, move):
        x, y = move
        if x < 0 or x >= 3 or y < 0 or y > 3:
            raise InvalidMoveException("Cell out of bounds: (%s, %s)" % (x, y))

        if game_state[x][y]:
            raise InvalidMoveException("Cell isn't free.")

    def execute_move(self, game_state, player, move):
        self.validate_move(game_state, player, move)

        self.moves.append([move, player])
        x, y = move
        game_state[x][y] = player
        next_player = self.get_other_player(player)
        return game_state, next_player

    def game_ended(self, game_state):
        if self.game_winner(game_state):
            return True
 
        for x in range(0, 3):
            for y in range(0, 3):
                if not game_state[x][y]:
                    return False
        return True

    def game_winner(self, game_state):
        """ Look for a game winner """
        # first look at all the lines
        for line in game_state:
             if line[0] == line[1] and line[1] == line[2]:
                return line[0]

        # check columns
        for col in range(0, 3):
            if (game_state[0][col] == game_state[1][col] and
                game_state[0][col] == game_state[2][col]):
                return game_state[0][col]

        # check diagonals
        if (game_state[0][0] == game_state[1][1] and
            game_state[0][0] == game_state[2][2]):
            return game_state[0][0]

        if (game_state[0][2] == game_state[1][1] and
            game_state[0][2] == game_state[2][0]):
            return game_state[0][2]

        # check if it's a tie
        tie = True
        for x in range(0, 3):
            for y in range(0, 3):
                if not game_state[x][y]:
                    tie = False
        if tie:
            return 'TIE'

        # No winner yet
        return None
