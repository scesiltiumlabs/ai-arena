from optparse import make_option

from django.core.management.base import BaseCommand

from eval.round_master import RoundMaster

class Command(BaseCommand):

    option_list = BaseCommand.option_list + (
        make_option('--round-id',
            dest='round_id',
            help='The id of the round we need to play'),
        )

    def handle(self, *args, **options):
        round_id = options.get('round_id')
        if not round_id:
            raise Exception("No round id provided")

        round_master = RoundMaster(round_id)
        round_master.play_round()
