import time
import simplejson as json

from django.core.management.base import BaseCommand
from django.conf import settings

from eval.game_master import GameMaster
from lib.queue import Queue

# Make this a param?
INPUT_QUEUE = 'game-master-queue'

class Command(BaseCommand):
    
    def handle(self, *args, **kwargs):
        queue = Queue(INPUT_QUEUE)
        while True:
            # listen to the queue and check if any match requests arrive
            message = queue.pop()
            if not message:
                time.sleep(1)
                continue

            print "Got a request to play a %s match between %s and %s" % \
                   (message['game'], message['player1'], message['player2'])
            game_master = GameMaster(message['game'], message['player1'], 
                                     message['player2'])
            winner = game_master.start_game()
            # write the result to the requested queue
            match_result = {
                'winner': winner,
                'match_log': game_master.match_log,
                'raw_match_log': game_master.raw_match_log
            }
            if hasattr(game_master, 'error_message'):
                match_result['error'] = game_master.error_message

            output_queue = Queue(message['result_queue'])
            output_queue.push(match_result)
