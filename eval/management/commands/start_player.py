from optparse import make_option
import time
import simplejson as json

from django.core.management.base import BaseCommand
from django.conf import settings

from eval.player import Player
from games.submission import SubmissionModel

class Command(BaseCommand):
    
    option_list = BaseCommand.option_list + (
        make_option('-s', '--submission-id',
            dest='submission_id',
            help='The id of the submission'),
        make_option('-q', '--queue',
            dest='queue',
            default='test-queue',
            help='The base name of the queues used')
        )

    def handle(self, *args, **options):
        # Retrieve and validate arguments
        submission_id = options.get('submission_id')
        if not submission_id:
            raise Exception("No submission_id provided")

        queue = options.get('queue')
        if not queue:
            raise Exception("No queue name provided")

        # Fetch the submission and start the player
        submission = SubmissionModel.objects.get(pk=submission_id)
        player = Player(queue, submission.id, submission.game.name)
        player.start_game() 
