import time

from lib.queue import Queue
from lib.jail import Jail, TimeoutException

SUBMISSIONS_IMPORT_PATH = 'eval.ais'

class Player(object):
    def __init__(self, queue_name, player_id, game):
        self.player_id = player_id
        self.game = game
        self.input_queue = Queue(queue_name + '-input')
        self.output_queue = Queue(queue_name + '-output')
        self.jail = Jail()
        self.user_ai = self.jail.execute(self.get_user_ai_class)

    def get_user_class_name(self):
        return self.game.capitalize() + 'Ai'

    def get_user_ai_class(self):
        user_class_name = self.get_user_class_name()
        module_path = '.'.join([SUBMISSIONS_IMPORT_PATH,
                                'player%s' % self.player_id,
                                '%s_ai' % self.game])
        try:
            module = __import__(module_path, globals(), locals(), [user_class_name])
            self.game_module = module
            klass = getattr(module, user_class_name, None)
            return klass()
        except Exception:
            # the ai class could not be imported, we will leave it to None and
            # when a move will come an error message will be sent to the 
            # game_master
            return None

    def start_game(self):
        """ This methods waits for a message from the game master. When a 
        message arrives the move method of the users ai is called and the
        result is sent to the game_master through the output queue.
        This process repeated until the process is killed by the game master.
        """
        while True:
            try:
                game_state = self.input_queue.pop()
                if not self.user_ai:
                    error_msg = 'Could not import ai. Please check class name'
                    self.output_queue.push({'error': error_msg})
                    continue

                move_start_time = time.time()
                move = self.jail.execute(self.user_ai.move, game_state)
                move_duration = time.time() - move_start_time

                result = {
                    'duration': move_duration,
                    'move': move
                }
                self.output_queue.push(result)
            except TimeoutException as e:
                self.output_queue.push({'error': 'Move time limit exceeded'})
                raise
            except Exception as e:
                # Most likely something happened inside the ai
                self.output_queue.push({'error': e.message})
                raise
