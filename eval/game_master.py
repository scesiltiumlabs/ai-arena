import simplejson as json
import subprocess
import sys
import random
import time

from games.models import GameModel
from lib.queue import Queue
from eval.exceptions import InvalidMoveException

class GameMaster(object):
    def __init__(self, game, player_id1, player_id2):
        self.game = game
        self.player1 = player_id1
        self.player2 = player_id2
        self.queues = {
            self.player1: self.get_player_queues(self.player1),
            self.player2: self.get_player_queues(self.player2)
        }

        self.game_rules = self.get_game_rules()
        self.match_log = {
            'players': [self.player1, self.player2],
            'game': self.game,
            'moves': []
        }
        self.raw_match_log = []
        self.player_process = {}

        # Get game options
        self.options = GameModel.objects.get(name=game).options

        self.player_time_left = {
            self.player1: self.options.total_time_limit,
            self.player2: self.options.total_time_limit
        }

    def get_game_rules(self):
        module = __import__('eval.game_rules.%s' % self.game, globals(),
                            locals(), [self.game.capitalize()])
        game_class = getattr(module, self.game.capitalize()) 
        return game_class([self.player1, self.player2])
 
    def get_player_queues(self, player_id):
        """ Build a queue name which should be pretty unique so that
        we make sure there are not two games running at the same time using
        the same queue. With that queue name create two queues (one for input
        and one for output) and return a dict with them to be used for
        communicating with the game master
        """
        queue_name = '%s-%s-%s-%s' % (self.game, player_id, int(time.time()), 
                                int(random.random() * 1000000))
        return {
            'root_name': queue_name,
            'input': Queue(queue_name + '-input'),
            'output': Queue(queue_name + '-output')
        }

    def write_message(self, player_id, message):
        """ Send a message to a player """
        queue = self.queues[player_id]['input']
        queue.push(message)

    def flush_queues(self):
        queue_list = [self.queues[self.player1]['input'],
                      self.queues[self.player1]['output'],
                      self.queues[self.player2]['input'],
                      self.queues[self.player2]['output']]
        for queue in queue_list:
            queue.flush()

    def get_message(self, player_id):
        """ Wait for the player to put a message on it's queue """
        queue = self.queues[player_id]['output']
        message = queue.pop()
        return message

    def start_player(self, player):
        queue_name = self.queues[player]['root_name']
        command = './manage.py start_player -s %s -q %s' % (player, queue_name)
        self.player_process[player] = subprocess.Popen(command, shell=True)

    def stop_player(self, player):
        self.player_process[player].kill()

    def invalid_ai(self, player, error_message):
        # This player's ai is invalid or made an invalid move. The other
        # player is declared winner and an error message is stored.
        winner = self.player1
        if player == self.player1:
            winner = self.player2

        # Retrieve match log
        self.match_log['moves'] = self.game_rules.get_match_log()

        self.error_message = error_message
        return winner

    def start_game(self):
        print "Game started"
        self.flush_queues()

        # Start players
        self.start_player(self.player1)
        self.start_player(self.player2)

        # Start game loop
        game_state = self.game_rules.get_initial_state()
        next_player = self.player1
        while True:
            # send the player a message and wait for it's move
            self.write_message(next_player, game_state)
            message = self.get_message(next_player)

            if 'error' in message:
                # user ai crashed
                self.raw_match_log.append({next_player: message['error']})
                return self.invalid_ai(next_player, message['error'])

            duration = message['duration']
            move = message['move']

            # Execute the move and get the next player
            previous_player = next_player
            try:
                game_state, next_player = self.game_rules.execute_move(
                                                game_state, next_player, move)
            except InvalidMoveException as e:
                # Player performed an invalid move
                self.raw_match_log.append({previous_player: move,
                                           'master': e.message})
                return self.invalid_ai(previous_player, e.message)

            # Check if the player hasn't exceeded it's total move time
            self.player_time_left[previous_player] -= duration
            if self.player_time_left[previous_player] < 0:
                self.raw_match_log.append({previous_player: move,
                                           'master': 'Total time limit exceeded'})
                return self.invalid_ai(previous_player, 'Total time limit exceeded') 

            # Move is valid - write in the raw match log 
            self.raw_match_log.append({previous_player: move,
                                       'master': 'OK!'})

            # Check if the game ended
            if self.game_rules.game_ended(game_state):
                winner = self.game_rules.game_winner(game_state)
                self.match_log['moves'] = self.game_rules.get_match_log()
                print "Player %s wins" % winner
                self.stop_player(self.player1)
                self.stop_player(self.player2) 
                return winner
