from operator import itemgetter
import copy
import hashlib
import math
import os
import random
import simplejson as json
import socket
import sys
import time

from games.models import RoundModel, SubmissionModel, MatchHistoryModel
from lib.redis_client import RedisClient
from lib.rating_calculator import RatingCalculator
from lib.queue import Queue

# This denotes an approximate number of the players to belong to a group
PLAYERS_PER_GROUP = 100
MIN_PLAYERS_PER_GROUP = 10

class RoundMaster(object):
    def __init__(self, round_id):
        self.round_id = round_id
        self.round = self.get_round(round_id)
        self.game = self.round.game.name
        self.scores = []

        # Init rating calculator
        self.rating_calculator = RatingCalculator()
 
        # init queues
        self.input_queue = self.get_input_queue()
        self.output_queue = self.get_output_queue()

        # flush queues for any leftover garbage
        self.input_queue.flush()
        self.output_queue.flush()

        # Init results
        self.results = {}

    def get_round(self, round_id):
        return RoundModel.objects.get(pk=round_id)

    def get_output_queue(self):
        return Queue("game-master-queue")

    def get_input_queue(self):
        """ This queue will be used to read the results of the matches which
        will be sent by the game master. We will have to send the name of this
        queue on the input queue when asking a game master to play a game,
        this way the game_master knows where to write the result of the match.
        This needs to be pretty unique so that concurrent round masters don't
        interfere with each other.
        """
        host_hash = hashlib.sha224(socket.gethostname()).hexdigest()
        queue_name = "round-%s-%s-%s-%s-input" % (host_hash, os.getpid(),
                                            int(time.time()), 
                                            int(random.random() * 1000000))
        return Queue(queue_name)

    def get_submissions(self):
        submissions = self.round.submissions.all()
        random.shuffle(list(submissions))
        return submissions

    def get_message(self):
        message = self.input_queue.pop()
        return message

    def write_message(self, message):
        print "Requesting a %s match to be played between %s and %s" % \
              (self.game, message['player1'], message['player2'])
        self.output_queue.push(message)

    def save_match_log(self, players, match_log):
        """ Save the match log in redis. This is useful to be able to show
        users the replay of the match.
        """
        redis_key = "%s-%s-%s-%s-%s" % (self.game, self.round_id, self.stage, 
                                        min(players), max(players))
        redis = RedisClient()
        redis.set(redis_key, match_log)
 
    def play_match(self, submission1, submission2):
        message = {
            'player1': submission1,
            'player2': submission2,
            'game': self.game,
            'result_queue': self.input_queue.name
        }
        
        self.write_message(message)
        match_result = self.get_message()
        print "Match winner: %s" % match_result['winner']

        self.save_match_log([submission1, submission2], 
                             match_result['match_log'])
        return match_result['winner']

    def log_group(self, submissions, completed_matches):
        print "Played group with players: %s" % ([submission.name for submission in submissions])
        print "Matches log:"
        submissions_dict = {}
        for submission in submissions:
            submissions_dict[submission.id] = submission

        for match, winner in completed_matches:
            print "%s vs %s -> %s" % (match[0], match[1], winner)

            # Save the match in match history
            winner_submission = None
            if winner != 'TIE':
                winner_submission = submissions_dict[winner]

            match_history = MatchHistoryModel(round=self.round)
            match_history.winner = winner_submission
            match_history.save()
            match_history.submissions.add(submissions_dict[match[0]])
            match_history.submissions.add(submissions_dict[match[1]])            

    def update_ratings(self, submissions):
        """ Updates the ratings of the submissions after a group has been 
        played. Receives a list of submission model a list of completed matches.
        This method assumes the fact that all matches are done and
        self.results is final.
        """
        # Retrieve matches from the results
        matches = []
        for stage in self.results:
            for group in self.results[stage]:
                matches.extend(group['matches'])

        player_ratings = {}
        submissions_dict = {}
        for submission in submissions:
            # We need to reload the ratings here as they might have been
            # updated previously without reloading the submissions
            player_ratings[submission.id] = \
                    SubmissionModel.objects.get(pk=submission.id).get_rating()
            submissions_dict[submission.id] = submission

        new_ratings = self.rating_calculator.compute_new_ratings(
                                             player_ratings, matches)

        for submission_id, new_rating in new_ratings.iteritems():            
            submissions_dict[submission_id].set_rating(
                    new_rating, 'Round %d' % self.round.index, self.round.date)
            submissions_dict[submission_id].save()

    def play_group(self, submissions):
        matches = []
        completed_matches = []

        # Build the list of matches
        for i in range(0, len(submissions)):
            for j in range(i + 1, len(submissions)):
               matches.append((submissions[i].id, submissions[j].id)) 

        # Play all the macthes
        for match in matches:
            winner = self.play_match(match[0], match[1])
            completed_matches.append([match, winner])

        self.log_group(submissions, completed_matches)
        return completed_matches

    def get_groups(self, submissions):
        """ Separate submissions into groups. """
        group_size = min(PLAYERS_PER_GROUP, int(math.sqrt(len(submissions))))
        group_size = max(group_size, MIN_PLAYERS_PER_GROUP)

        groups = []
        current_group = []
        for submission in submissions:
            current_group.append(submission)
            if len(current_group) == group_size:
                groups.append(current_group)
                current_group = []

        if len(groups) == 0:
            # There is only one group and it's not really complete
            groups.append(current_group)
            return groups
 
        # spread the last group through the other groups
        group_index = 0
        for submission in current_group:
            groups[group_index].append(submission)
            group_index = (group_index + 1) % len(groups)

        return groups

    def get_winners(self, submissions, scores, final_stage=False):
        points = {}
        submissions_dict = {}
        for submission in submissions:
            points[submission.id] = 0
            submissions_dict[submission.id] = submission

        for match, winner in scores:
            if winner == 'TIE':
                continue

            points[winner] += 1
            loser = match[0]
            if winner == match[0]:
                loser = match[1]
            points[loser] -= 1

        standings = sorted(points.iteritems(), key=itemgetter(1), reverse=True)
        cutoff_index = len(standings) - len(standings) / 2 - 1
        cutoff = standings[cutoff_index][1]
        if final_stage:
            cutoff = standings[0][1]

        winners = []
        for submission_id, points in standings:
            if points >= cutoff:
                winners.append(submissions_dict[submission_id])

        group_results = {
            'matches': scores,
            'standings': []
        }

        print "Match Standings:"
        for submission, points in standings:
            standing_row = [submission, points, False]
            qualified_mark = ''
            if points >= cutoff:
                qualified_mark = '*'
                standing_row[2] = True

            group_results['standings'].append(standing_row)
            print "%s %s %s" % (submission, points, qualified_mark)

        if self.stage not in self.results:
            self.results[self.stage] = []
       
        self.results[self.stage].append(group_results)
        return winners

    def write_results(self, results):
        print "Updating results"
        self.round.results = json.dumps(results)
        self.round.save()        

    def play_round(self):
        self.submissions = self.get_submissions()
        current_submissions = copy.copy(self.submissions)
        self.stage = 1
        while True:
            print "Starting stage %d" % self.stage

            groups = self.get_groups(current_submissions)
            final_stage = len(groups) == 1

            current_submissions = []
            for group in groups:
                scores = self.play_group(group)
                current_submissions.extend(self.get_winners(group, scores, 
                                                            final_stage))

            if final_stage:
                # This was the final group and we're done
                self.write_results(self.results)
                self.update_ratings(self.submissions)
                return

            # We still have to go on for a new group stage
            self.stage += 1
 
if __name__ == '__main__':
    round_master = RoundMaster(sys.argv[1])
    round_master.play_round()
