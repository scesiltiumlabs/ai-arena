from lib.queue import Queue

def play_match(player1_id, player2_id, game):
    input_queue = Queue('game-master-queue')
    output_queue = Queue('test-result-queue')
    input_queue.push({
        'player1': player1_id,
        'player2': player2_id,
        'game': game,
        'result_queue': output_queue.name
    })

    return output_queue.pop()
