from datetime import datetime, timedelta

from django.db import models

class GameModel(models.Model):
    class Meta:
        app_label = 'games'
        db_table = 'game'

    name = models.CharField(max_length=255)
    description = models.CharField(max_length=512, default='')
    has_image = models.BooleanField(default=False)
    sample_submission = models.ForeignKey('SubmissionModel', null=True)
    options = models.ForeignKey('GameOptionsModel', null=True)

    @staticmethod
    def get_games_list(limit=50):
        """ Returns a list of games.
        """
        games = GameModel.objects.all()[:limit]
        return games

    def get_submissions(self):
        from games.models import SubmissionModel
        return SubmissionModel.objects.filter(game=self)

    def get_statement(self, version=None):
        try:
            version = int(version)
        except Exception:
            version = None

        if version and version >= self.versions.count():
            # Invalid version number
            version = None

        if self.versions.count() == 0:
            # No versions yet
            return ''

        if version is None:
            # Get latest version
            game_version = self.versions.order_by('-id')[0]
            return game_version.statement
        else:
            # Return a specific version
            game_version = self.versions.order_by('id')[version]
            return game_version.statement

    def create_new_version(self, user, statement):
        from games.models import GameVersionModel
        version = GameVersionModel(statement=statement, created_by=user,
                                   game=self)
        version.save()

    def get_image_src(self):
        if not self.has_image:
            return '/static/images/no-image.jpeg'
        else:
            return '/static/game_images/%s.jpeg' % self.name

    def get_rounds(self):
        from games.models import RoundModel
        return RoundModel.objects.filter(game=self)

    def next_round_date(self):
        """ Next monday at 12:00 """
        # Get a datetime for next monday and the same hour as now
        today = datetime.today()
        next_monday = today + timedelta(days=7 - today.weekday())
        # Now normalize it to 12:00
        seconds = next_monday.second + next_monday.minute * 60 + \
                  next_monday.hour * 3600
        next_monday = next_monday - timedelta(seconds=seconds - 12 * 3600)
        return next_monday
