from django.db import models

class GameOptionsModel(models.Model):
    class Meta:
        app_label = 'games'
        db_table = 'game_options'

    move_time_limit = models.IntegerField(help_text='In seconds')
    total_time_limit = models.IntegerField(help_text='In seconds')
    memory_limit = models.IntegerField(help_text='In megabytes')
