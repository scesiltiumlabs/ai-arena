import datetime
import os

from django.db import models
from django.contrib.auth.models import User

from games.game import GameModel

SUBMISSION_DEFAULT_RATING = 1200
SUBMISSIONS_ROOT_DIRECTORY = os.path.join('eval', 'ais')

class SubmissionModel(models.Model):
    class Meta:
        app_label = 'games'
        db_table = 'submission'

    user = models.ForeignKey(User)
    game = models.ForeignKey(GameModel)
    name = models.CharField(null=False, blank=False, max_length=256)
    status = models.CharField(default='active', max_length=32)
    rating = models.IntegerField(default=1200)
    created_at = models.DateField(auto_now_add=True, default=datetime.datetime.now)
    test_submission = models.BooleanField(null=False, blank=True, default=False)

    def get_name(self):
        # TODO: remove this and replace it directly with submission.name
        return self.name

    def get_rating(self):
        if self.ratings.count() == 0:
            return SUBMISSION_DEFAULT_RATING

        return self.ratings.order_by('-date')[0].value

    def get_match_history(self):
        return list(self.matches.all())

    def get_rating_history(self):
        # Each submission starts with a default rating at first
        rating_history = [(SUBMISSION_DEFAULT_RATING, self.created_at, '')]
        for rating in self.ratings.all():
            rating_history.append((rating.value, rating.date, 
                                   rating.description))

        return rating_history

    def set_rating(self, value, description='', date=None):
        from games.models import RatingHistoryModel
        self.rating = value

        # Save rating history
        if not date:
            date = datetime.datetime.now()
        rating = RatingHistoryModel(value=value, description=description,
                                    submission=self, date=date)
        rating.save()

    def get_submission_dirname(self):
        return 'player%d' % self.id

    def get_submission_filename(self):
        return self.game.name + '_ai.py'

    def is_zip_archive(self, source_file):
        return source_file.name.split('.')[-1] == 'zip'

    def save_source_file(self, source_file):
        zip_archive = self.is_zip_archive(source_file)

        submissions_dir = SUBMISSIONS_ROOT_DIRECTORY

        # The following steps will work directly with the filesystem. For this
        # reason each step is enclosed in a try catch to make sure we control
        # the entire procedure

        # Save original dir to revert back to it
        original_cwd = os.getcwd()

        # change dir to submissions dir
        try:
            os.chdir(submissions_dir)
        except OSError:
            # TODO: change this to logging when the logging system is done
            print "Couldn't change dir"
            raise

        # Create submission dir
        try:
            os.mkdir(self.get_submission_dirname())
            os.chdir(self.get_submission_dirname())
        except OSError:
            print "Couldn't initialize submission directory"
            raise

        if not zip_archive:
            os.system('touch __init__.py')
            os.system(self.get_submission_filename())
 
            # Dump source file contents
            submission_file = open(self.get_submission_filename(), "w")
            submission_file.write(source_file.read())
            submission_file.close()
        else:
            try:
                # Create archive file and unzip it
                os.system('touch source.zip')
                zip_file = open('source.zip', "w")
                zip_file.write(source_file.read())
                zip_file.close()
                os.system('unzip source.zip')
            except Exception:
                print "Couldn't unzip archive"
                raise

        # Revert back to the original working dir
        os.chdir(original_cwd)

        return True

