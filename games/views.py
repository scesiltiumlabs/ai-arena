import simplejson as json

from django.core.urlresolvers import reverse
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import user_passes_test, login_required
from django.contrib.auth.models import User
from django.forms import model_to_dict
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader, Context
from textile import textile

from games.models import RoundModel, SubmissionModel, GameModel
from games.user.register_form import RegisterForm
from games.permissions import is_superuser
from lib.redis_client import RedisClient

def home(request):
    games = GameModel.get_games_list()

    context = Context({
        'games': list(games),
        'request': request
    }) 
    template = loader.get_template('index.html')
    return HttpResponse(template.render(context))

def round_standings(request, game_name, round_index):
    game = GameModel.objects.get(name=game_name)

    round = RoundModel.objects.get(game=game, index=round_index)

    template = loader.get_template('round_standings.html') 
    context = Context({
        'request': request,
        'results': round.get_results(),
        'submissions_dict': round.get_submission_names_dict(),
        'round': round,
        'rounds': RoundModel.objects.filter(game=game),
        'game': game,
    })

    return HttpResponse(template.render(context))

def round_matches(request, game_name, round_index):
    game = GameModel.objects.get(name=game_name)
    round = RoundModel.objects.get(game=game, index=round_index)

    # We want to extract from the round results the list of played matches
    results = round.get_results()
    matches = {}
    for stage, groups in results.iteritems():
        matches[stage] = {}
        for group_id, group in enumerate(groups):
            matches[stage][group_id] = []
            for match in group['matches']:
                matches[stage][group_id].append(match[0])

    context = Context({
        'request': request,
        'matches': matches,
        'submissions_dict': round.get_submission_names_dict(),
        'round': round,
        'rounds': RoundModel.objects.filter(game=game),
        'game': game
    })
    template = loader.get_template('round_matches.html')

    return HttpResponse(template.render(context))

def match_log(request):
    redis = RedisClient()

    game_name = request.GET['game']
    round_id = request.GET['round_id']
    stage = request.GET['stage']
    player1 = request.GET['player1']
    player2 = request.GET['player2']

    match_redis_key = '%s-%s-%s-%s-%s' % (game_name, round_id, stage,
                                          min([player1, player2]), 
                                          max([player1, player2]))
    match_log = redis.get(match_redis_key)
    return HttpResponse(json.dumps(match_log))

def game_details(request, game_name):
    game = GameModel.objects.get(name=game_name)
    rounds = RoundModel.objects.filter(game=game)
    version = request.GET.get('version', None)
    if not request.user.is_superuser:
        version = None

    context = Context({
        'request': request,
        'game': game,
        'game_statement': textile(game.get_statement(version)),
        'rounds': rounds
    })

    template = loader.get_template('game_details.html')
    return HttpResponse(template.render(context))

def game_ratings(request, game_name):
    game = GameModel.objects.get(name=game_name)
    submissions = SubmissionModel.objects.filter(game=game).order_by('-rating')
    rounds = RoundModel.objects.filter(game=game) 
    context = Context({
        'request': request,
        'game': game,
        'rounds': list(rounds), 
        'submissions': list(submissions)
    })

    template = loader.get_template('game_ratings.html')
    return HttpResponse(template.render(context))

@login_required
def game_submissions(request, game_name):
    game = GameModel.objects.get(name=game_name)
    submissions = SubmissionModel.objects.filter(game=game, user=request.user)
    rounds = RoundModel.objects.filter(game=game)
    context = Context({
        'request': request,
        'game': game,
        'rounds': list(rounds),
        'submissions': list(submissions)
    })

    template = loader.get_template('game_submissions.html')
    return HttpResponse(template.render(context))

@login_required
def submissions_json(request, game_name):
    game = GameModel.objects.get(name=game_name)
    submissions = SubmissionModel.objects.filter(game=game, user=request.user)
    submissions_dict = [model_to_dict(submission) for submission in submissions]
    return HttpResponse(json.dumps(submissions_dict))

def login_page(request):
    context = Context({
        'request': request,
    })
    template = loader.get_template('login_page.html')
    return HttpResponse(template.render(context))

def logout_view(request):
    logout(request)
    next_url = request.GET.get('next_url', '/')
    return HttpResponseRedirect(next_url)

def login_view(request):
    username = request.POST['username']
    password = request.POST['password']
    next_url = request.POST.get('next_url', '/')

    user = authenticate(username=username, password=password)
    if user is not None:
        login(request, user)

    return HttpResponseRedirect(next_url)

def register(request):
    data = request.POST
    form = RegisterForm(data)
    if not form.is_valid():
        context = Context({
            'request': request,
            'register_errors': form.errors,
            'register_data': form.data,
            'login_errors': {},
            'login_data': {},
        })
        template = loader.get_template('login_page.html')
        return HttpResponse(template.render(context))
    else:
        # Save the user
        user = form.save()
        
        # Automatically authenticate the user register
        user = authenticate(username=user.username, 
                     password=form.cleaned_data['password1'])
        login(request, user)
        return HttpResponseRedirect('/')

@login_required(login_url='/')
def new_submission(request):
    # Create a new submission entry
    user = request.user
    game = GameModel.objects.get(id=request.POST['game'])
    submission = SubmissionModel(user=user, game=game, 
                                 name=request.POST['name'],
                                 status='pending')
    submission.save()
    submission.save_source_file(request.FILES['submission'])

    return HttpResponseRedirect(reverse('game-submissions', args=[game.name]))

@user_passes_test(is_superuser, login_url='/')
def edit_game_details(request, game):
    game = GameModel.objects.get(name=game)
    rounds = RoundModel.objects.filter(game=game)
 
    template = loader.get_template('edit_game_details.html')
    context = Context({
        'request': request,
        'game': game,
        'rounds': rounds,
    })
    return HttpResponse(template.render(context))

@user_passes_test(is_superuser, login_url='/')
def save_game_details(request, game):
    game = GameModel.objects.get(name=game)
    game.create_new_version(request.user, request.POST['statement'])

    return HttpResponseRedirect(reverse('game-details', args=[game.name]))

@user_passes_test(is_superuser, login_url='/')
def game_history(request, game):
    game = GameModel.objects.get(name=game)
    rounds = RoundModel.objects.filter(game=game)

    template = loader.get_template('view_history.html')
    context = Context({
        'request': request,
        'game': game,
        'rounds': rounds,
        'versions': list(game.versions.all())
    })

    return HttpResponse(template.render(context))

def submission_info(request, game, submission_id):
    game = GameModel.objects.get(name=game)
    rounds = RoundModel.objects.filter(game=game)
    submission = SubmissionModel.objects.get(pk=submission_id)
 
    # Compute match history
    matches = submission.get_match_history()
    winrate_history = []
    wins = 0
    total = 0
    for match in matches:
        if match.winner == submission:
            wins = wins + 1
        total += 1
        match_title = '%s vs %s' % (match.submissions.all()[0].get_name(),
                                    match.submissions.all()[1].get_name())
        winrate_history.append((1.0 * wins / total, match_title))

    # Get raw match log of the test match ran against the sample submission
    # Only needed for inactive submissions
    if submission.status == 'inactive':
        redis_client = RedisClient()
        redis_key = 'test-submission-%s' % submission.id
        raw_match_log = redis_client.get(redis_key).get('raw_match_log')

        # Process the match log a bit - need to remap some keys
        key_remap = {
            str(game.sample_submission_id): 'sample',
            str(submission.id): 'user_submission',
        }
        for row in raw_match_log:
            for key, value in key_remap.iteritems():
                if key in row:
                    row[value] = row[key]
                    del row[key]
            print row
    else:
        raw_match_log = None

    print raw_match_log
    template = loader.get_template('submission_info.html')
    context = Context({
        'request': request,
        'game': game,
        'rounds': rounds,
        'round_count': submission.rounds.all().count(),
        'match_count': len(matches),
        'winrate_history': winrate_history,
        'raw_match_log': raw_match_log,
        'submission': submission
    })
    return HttpResponse(template.render(context))
