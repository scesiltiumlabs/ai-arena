import simplejson as json
from urllib import urlencode

from django import template
from django.core.urlresolvers import reverse

register = template.Library()

@register.filter
def iterate(x):
    return xrange(0, x)

@register.filter
def to_json(obj):
    ret = json.dumps(obj);
    print ret
    return ret

@register.filter
def get_name(id, names_dict):
    return names_dict[id]

@register.simple_tag
def build_url(url_name, *args, **kwargs):
    base_url = reverse(url_name, args=args)
    return base_url + '?' + urlencode(kwargs)

@register.simple_tag
def enum(object_list):
    return enumerate(object_list)
