function create_rating_chart(container_id, title, data) {
    $('#' + container_id).highcharts({
        chart: {
            type: 'line',
            marginRight: 130,
            marginBottom: 25,
        },
        title: {
            text: title,
        },
        plotOptions: {
            line: {
                animation: true
            },
        },
        tooltip: {
            enabled: true
        },
        xAxis: {
            type: 'datetime',
            title: {
                text: null
            }
        },
        legend: {
            enabled: false
        },
        yAxis: {
            title: {
                text: 'Rating',
            },
        },
        colors: ['#900'],
        series: [{
            type: 'line',
            name: 'Rating',
            data: data
        }]
    })
}

 function create_winrate_chart(container_id, title, data) {
    $('#' + container_id).highcharts({
        chart: {
            type: 'line',
            marginRight: 130,
            marginBottom: 25,
        },
        title: {
            text: title,
        },
        plotOptions: {
            line: {
                animation: true
            },
        },
        tooltip: {
            enabled: true
        },
        legend: {
            enabled: false
        },
        yAxis: {
            title: {
                text: 'Winrate %',
            },
            min: 0,
            max: 1,
        },
        colors: ['#900'],
        series: [{
            type: 'line',
            name: 'Rating',
            data: data
        }]
    })
} 
