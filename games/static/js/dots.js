var players_char = {};
var players_color = {};

function apply_move(move) {
    var cell_id = String(move["cell"][0]) + String(move["cell"][1]);
    var border = move["border"];

    cell = $("#" + cell_id)[0]
    player_color = players_color[move['player']]
    if (border == 'bottom') {
        cell.style.borderBottom = '2px solid ' + player_color;
    } else if (border == 'top') {
        cell.style.borderTop = '2px solid ' + player_color; 
    } else if (border == 'left') {
        cell.style.borderLeft = '2px solid ' + player_color;
    } else if (border == 'right') {
        cell.style.borderRight = '2px solid ' + player_color;
    }

    for (i in move["captures"]) {
        cell = move["captures"][i];
        div = $("#" + String(cell[0]) + String(cell[1]))[0]
        div.style.color = players_color[move["player"]]
        div.innerHTML = players_char[move["player"]];
    }
}

function undo_move(move) {
    var cell_id = String(move["cell"][0]) + String(move["cell"][1]);
    var border = move["border"];

    cell = $("#" + cell_id)[0]
    if (border == 'bottom') {
        cell.style.borderBottom = '2px solid #eee';
    } else if (border == 'top') {
        cell.style.borderTop = '2px solid #eee'; 
    } else if (border == 'left') {
        cell.style.borderLeft = '2px solid #eee';
    } else if (border == 'right') {
        cell.style.borderRight = '2px solid #eee';
    }

    for (i in move["captures"]) {
        cell = move["captures"][i];
        div = $("#" + String(cell[0]) + String(cell[1]))[0]
        div.innerHTML = '';
    }
}

function reset_match() {


}

function init_match(players) {
    players_char[players[0]] = 'X';
    players_char[players[1]] = '0';

    players_color[players[0]] = 'red';
    players_color[players[1]] = 'blue';
}
