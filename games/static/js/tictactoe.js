var players_char = {};
var players_color = {};

function apply_move(move) {
    var div_id = String(move[0][0]) + String(move[0][1]);
    $('#' + div_id)[0].style.color = players_color[move[1]]
    $('#' + div_id)[0].innerHTML = players_char[move[1]];
}

function undo_move(move) {
    var div_id = String(move[0][0]) + String(move[0][1]);
    $('#' + div_id)[0].innerHTML = '';
}

function reset_match() {
    for (var i = 0; i < 3; i++) {
        for (var j = 0; j < 3; j++) {
            $('#' + String(i) + String(j))[0].innerHTML = '';
        }
    }
}

function init_match(players) {
    players_char[players[0]] = 'X';
    players_char[players[1]] = '0';

    players_color[players[0]] = 'red';
    players_color[players[1]] = 'blue';
}
