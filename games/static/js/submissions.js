SYNC_REFRESH_RATE = 5000;

String.prototype.capitalize = function() { 
   return this.toLowerCase().replace(/^.|\s\S/g, function(a) { return a.toUpperCase(); });
}

function update_submissions(data) {
    submissions = JSON.parse(data)
    for (i = 0; i < submissions.length; i++) {
        submission = submissions[i];
        // Get row
        console.log(submission);
        console.log('#' + submission['id']);
        submission_row = $('#' + submission['id'])[0]

        // update status
        status_cell = submission_row.children[2]
        status_cell.className = 'submission-status-' + submission['status'];
        status_cell.innerHTML = '';
        if (submission['status'] == 'pending') {
            status_cell.innerHTML += '<img src="/static/images/loader.gif" /> ';
        }
        status_cell.innerHTML += submission['status'].capitalize(); 
    }
}

function sync_submissions() {
    $.get('/tictactoe/get_submissions', {}, update_submissions);
}

setInterval(sync_submissions, SYNC_REFRESH_RATE);
