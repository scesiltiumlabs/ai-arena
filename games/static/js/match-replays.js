active_stage = null;
active_match = null;

function init_replay_interface() {
    // Init the group accordion to open the first stage by default
    $('#stage-slide-1')[0].style.display = 'block';
    active_stage = 1;

    // Init the replay interface to load up the first match
    match_id = $('#match-list .match-entry:first')[0].id;
    load_match(match_id);
}

function toggle_stage(stage_id) {
    if (active_stage != null && active_stage != stage_id) {
        $('#stage-slide-' + active_stage).animate({width: 'toggle'}, 1000)
    }

    $('#stage-slide-' + stage_id).animate({width: 'toggle'}, 1000);
    if (active_stage == stage_id) {
        active_stage = null;
    } else {
        active_stage = stage_id;
    }
}

var match_log = undefined;
var current_move = 0;

function get_match_row_html(stage, match) {
    id = String(stage) + '-' + match[0] + '-' + match[1];
    onclick_function = "load_match('" + stage + "', '" + match[0] + "', '" +
                                        match[1] + "')";

    return "<tr class='match-entry' style='opacity: 0' id='" + id + "'" + 
           "    onclick=\"" + onclick_function + "\" >" +
           "        <td>" + player_names[match[0]] + "</td>" +
           "        <td>vs</td>" +
           "        <td>" + player_names[match[1]] + "</td>" +
           "</tr>";
}

function change_group(stage, group) {
    $('#match-list .match-entry').animate({opacity: 0}, 400)
    $('#match-list .match-entry').promise().done(function() {
        $('#match-list .match-entry').remove();
        for (i = 0; i < matches[stage][group].length; i++) {
            match = matches[stage][group][i];
            $('#match-list tr:last').after(get_match_row_html(stage, match));
        }
        $('#match-list .match-entry').animate({opacity: 1}, 400) 
    });
}

function load_match_callback(data) {
    match_log = JSON.parse(data); 
    current_move = 0;
    init_match(match_log['players']);

    // Change match title
    player1_name = player_names[match_log['players'][0]]
    player2_name = player_names[match_log['players'][1]] 
    $('#match-replay-title')[0].innerHTML = player1_name + ' vs ' + player2_name;
}

function activate_match(match_id) {
    if (active_match != null && active_match != match_id) {
        $('#' + active_match)[0].style.backgroundColor = 'inherit';
    }
    active_match = match_id;
    $('#' + active_match)[0].style.backgroundColor = '#FFFFDF'; 
}

function load_match(match_id) {
    reset_match(); 
    activate_match(match_id)

    match_parts = match_id.split('-');
    data = {
        game: game,
        round_id: round_id,
        stage: match_parts[0],
        player1: match_parts[1],
        player2: match_parts[2]
    }
    $.get('/match-log/', data, load_match_callback);
}

function next_move() {
    if (current_move < match_log["moves"].length) {
        apply_move(match_log["moves"][current_move]);
        current_move += 1;
    }
}

function previous_move() {
   if (current_move >= 1) {
        current_move -= 1;
        undo_move(match_log["moves"][current_move]);
    }
}

$(document).ready(function() {
    init_replay_interface();
})

