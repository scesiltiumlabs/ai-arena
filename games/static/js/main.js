function toggle_matches(id) {
    link = $('#toggle-detailed-matches-link-' + id)[0];
    if (link.innerHTML == 'Hide Matches') {
        link.innerHTML = 'View Matches';
    } else {
        link.innerHTML = 'Hide Matches';
    }

    var container_id = 'detailed-matches-' + id;
    $("#" + container_id).animate({height: 'toggle'}, 400);
}

function toggle_round_submenu(id) {
    var container_id = 'round-submenu-' + id;
    $('#' + container_id).animate({height: 'toggle'}, 400);
}


