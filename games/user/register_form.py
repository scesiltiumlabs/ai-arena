from django import forms

from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

class RegisterForm(UserCreationForm):
    """ Form based on django's UserCreationForm to validate and create new
    users. 
    Differences between this form and the UserCreationForm:
    * email is a required field
    * username character list is a bit more restrictive
    * password must contain at least 5 characters
    """
    MIN_PASSWORD_LENGTH = 5

    class Meta(UserCreationForm.Meta):
        fields = ("username", "password1", "password2", "email")

    username = forms.RegexField(max_length=30, regex=r'^[\w\-.]+$')
    email = forms.CharField(max_length=255, required=True)

    def clean_password1(self):
        password = self.data.get('password1')
        if len(password) < self.MIN_PASSWORD_LENGTH:
            raise forms.ValidationError('Password too short')

        return password
