from django.db import models
from django.contrib.auth.models import User

class GameVersionModel(models.Model):
    class Meta:
        app_label = 'games'
        db_table = 'game_versions'

    game = models.ForeignKey('GameModel', related_name='versions')
    statement = models.TextField(default='')
    created_at = models.DateField(auto_now_add=True)
    created_by = models.ForeignKey(User)
