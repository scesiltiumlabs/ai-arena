from django.db import models

class MatchHistoryModel(models.Model):
    class Meta:
        app_label = 'games'
        db_table = 'match_history'
 
    submissions = models.ManyToManyField('SubmissionModel', related_name='matches')
    winner = models.ForeignKey('SubmissionModel', related_name='matches_won', null=True, blank=True)
    round = models.ForeignKey('RoundModel', related_name='matches')
