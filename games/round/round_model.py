import datetime
import simplejson as json

from django.db import models

from games.game import GameModel
from games.submission import SubmissionModel

class RoundModel(models.Model):
    class Meta:
        app_label = 'games'
        db_table = 'round'

    index = models.IntegerField()
    game = models.ForeignKey(GameModel)
    submissions = models.ManyToManyField(SubmissionModel, related_name='rounds')
    date = models.DateTimeField(auto_now_add=True, default=datetime.datetime.now)
    results = models.TextField()

    def get_results(self):
        if not self.results:
            return None

        results = json.loads(self.results)
        
        # Give each group a unique_id
        for stage, groups in results.iteritems():
            cnt = 0
            for group in groups:
                group['unique_id'] = '%s-%s' % (stage, cnt)
                cnt += 1

        # Sometimes stages come in a weird order so we are ordering them here
        # because the dicts lose the keys order when they are jsoned
        ordered_results = {}
        for stage in sorted(results.keys(), key=lambda x: int(x)):
            ordered_results[int(stage)] = results[stage]

        return ordered_results

    def get_submission_names_dict(self):
        """ Returns a dict containing the submissions sent to this round
        indexed by their id.
        """
        submission_dict = {}
        for submission in self.submissions.all():
            submission_dict[submission.id] = submission.get_name()

        return submission_dict


