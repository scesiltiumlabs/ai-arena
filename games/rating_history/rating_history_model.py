import datetime

from django.db import models

class RatingHistoryModel(models.Model):
    class Meta:
        app_label = 'games'
        db_table = 'rating_history'

    value = models.IntegerField(default=1200)
    submission = models.ForeignKey('SubmissionModel', related_name='ratings')
    date = models.DateField(null=False, default=datetime.datetime.now)
    description = models.CharField(max_length=255,
                                   help_text='what caused the rating to change')
