from optparse import make_option

from django.core.management.base import BaseCommand, CommandError

from games.models import GameModel, SubmissionModel, RoundModel

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--game',
            dest='game',
            help='For what game do you want to create a new round'),
        )

    def handle(self, *args, **options):
        game_name = options.get('game')
        if not game_name:
            raise Exception("No game name provided")

        game = GameModel.objects.get(name=game_name)
        # Create empty round   
        index = RoundModel.objects.filter(game=game).count() + 1
        new_round = RoundModel(index=index, game=game)
        new_round.save()

        for submission in SubmissionModel.objects.filter(game=game, status='active'):
            new_round.submissions.add(submission)

        print "Created new round for %s" % game
        print "Round id: %s\tNumber of submissions %s" % (
                    new_round.id, new_round.submissions.count())
