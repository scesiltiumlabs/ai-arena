import time

from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

from games.models import SubmissionModel
from lib.queue import Queue
from lib.redis_client import RedisClient

SLEEP_INTERVAL = 1

class Command(BaseCommand):
    input_queue = Queue('submission-test-cron-queue')
    game_master_queue = Queue('game-master-queue')

    def get_redis_key(self, submission):
        return 'test-submission-%s' % submission.id
 
    def test_submission(self, submission):
        sample_submission = submission.game.sample_submission
        if not sample_submission:
            # No sample submission defined for this game so no checking will
            # be done
            return True

        match_request = {
            'player1': submission.id,
            'player2': sample_submission.id,
            'game': submission.game.name,
            'result_queue': self.input_queue.name
        }
        self.game_master_queue.push(match_request)
        match_result = self.input_queue.pop()

        # Save match log in redis
        redis_key = self.get_redis_key(submission)
        self.redis_client.set(redis_key, match_result)
        if 'error' not in match_result:
            return True
        else:
            return False

    @transaction.commit_manually
    def handle(self, *args, **options):
        self.redis_client = RedisClient()
        while True:
            submissions = SubmissionModel.objects.filter(status='pending')

            for submission in submissions:
                print "Testing submission %s..." % (submission.id),
                if self.test_submission(submission):
                    submission.status = 'active'
                    print "OK"
                else:
                    submission.status = 'inactive'
                    print "Invalid"
                submission.save()

            transaction.commit()
            time.sleep(SLEEP_INTERVAL)
