-- MySQL dump 10.13  Distrib 5.6.10, for osx10.8 (x86_64)
--
-- Host: localhost    Database: aiarena
-- ------------------------------------------------------
-- Server version	5.5.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_bda51c3c` (`group_id`),
  KEY `auth_group_permissions_1e014c8f` (`permission_id`),
  CONSTRAINT `group_id_refs_id_3cea63fe` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `permission_id_refs_id_a7792de1` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_e4470c6e` (`content_type_id`),
  CONSTRAINT `content_type_id_refs_id_728de91f` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add permission',1,'add_permission'),(2,'Can change permission',1,'change_permission'),(3,'Can delete permission',1,'delete_permission'),(4,'Can add group',2,'add_group'),(5,'Can change group',2,'change_group'),(6,'Can delete group',2,'delete_group'),(7,'Can add user',3,'add_user'),(8,'Can change user',3,'change_user'),(9,'Can delete user',3,'delete_user'),(10,'Can add content type',4,'add_contenttype'),(11,'Can change content type',4,'change_contenttype'),(12,'Can delete content type',4,'delete_contenttype'),(13,'Can add session',5,'add_session'),(14,'Can change session',5,'change_session'),(15,'Can delete session',5,'delete_session'),(16,'Can add site',6,'add_site'),(17,'Can change site',6,'change_site'),(18,'Can delete site',6,'delete_site'),(19,'Can add game model',7,'add_gamemodel'),(20,'Can change game model',7,'change_gamemodel'),(21,'Can delete game model',7,'delete_gamemodel'),(22,'Can add log entry',8,'add_logentry'),(23,'Can change log entry',8,'change_logentry'),(24,'Can delete log entry',8,'delete_logentry'),(25,'Can add submission model',9,'add_submissionmodel'),(26,'Can change submission model',9,'change_submissionmodel'),(27,'Can delete submission model',9,'delete_submissionmodel'),(28,'Can add round model',10,'add_roundmodel'),(29,'Can change round model',10,'change_roundmodel'),(30,'Can delete round model',10,'delete_roundmodel'),(31,'Can add migration history',11,'add_migrationhistory'),(32,'Can change migration history',11,'change_migrationhistory'),(33,'Can delete migration history',11,'delete_migrationhistory'),(34,'Can add game version model',12,'add_gameversionmodel'),(35,'Can change game version model',12,'change_gameversionmodel'),(36,'Can delete game version model',12,'delete_gameversionmodel'),(37,'Can add rating history model',13,'add_ratinghistorymodel'),(38,'Can change rating history model',13,'change_ratinghistorymodel'),(39,'Can delete rating history model',13,'delete_ratinghistorymodel'),(40,'Can add match history model',14,'add_matchhistorymodel'),(41,'Can change match history model',14,'change_matchhistorymodel'),(42,'Can delete match history model',14,'delete_matchhistorymodel'),(43,'Can add game options model',15,'add_gameoptionsmodel'),(44,'Can change game options model',15,'change_gameoptionsmodel'),(45,'Can delete game options model',15,'delete_gameoptionsmodel');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `password` varchar(128) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `last_login` datetime NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'tibi','','','tiberiu.savin@gmail.com','pbkdf2_sha256$10000$jNylZHYJIa59$h3vrv1UO9tXkZk6JH76q/zdpHBVNC4tn8QzNWCaiJ2U=',1,1,1,'2013-06-22 12:12:03','2013-04-09 02:13:44'),(2,'tibithegreat','','','tiberiu.savin@gmail.com','pbkdf2_sha256$10000$FsCQLUDnlbxF$s+eADY1fE68/NcbpyoeFO7nY2vqkR/n4KfoD1LBHUuk=',0,1,0,'2013-05-20 03:35:28','2013-05-20 03:34:53'),(3,'tibithegreat2','','','tiberiu.savin@gmail.com','pbkdf2_sha256$10000$3UV0efXTMVmQ$/IDNfk6i7t4Ek0UPubsdBPnI83vHBtbSpi9r/O77uOk=',0,1,0,'2013-06-10 22:29:03','2013-05-20 03:42:25'),(4,'tibithegreat3','','','tiberiu.savin@gmail.com','pbkdf2_sha256$10000$R4BXU73VkEOh$Dyl3tn9bRgJqia6wliYA7cHanBTHUQyx4eCIZ74MSXM=',0,1,0,'2013-05-20 03:43:47','2013-05-20 03:43:47'),(5,'tibithegreat4','','','tiberiu.savin@gmail.com','pbkdf2_sha256$10000$bhZ5ysSvlHTd$skhFRtQJTud0Qr+gdw4ovjugx3tS/2b6UadizHxOCGs=',0,1,0,'2013-05-20 03:44:23','2013-05-20 03:44:23'),(6,'tibithegreat5','','','tiberiu.savin@gmail.com','pbkdf2_sha256$10000$ANkZUNuQBUKM$tuQXTJ7UaCwCVB0sbwlFFRpYD9cbsSP78XY5TSC5CZg=',0,1,0,'2013-05-20 03:47:31','2013-05-20 03:47:31'),(7,'portocala','','','orange.alex01@gmail.com','pbkdf2_sha256$10000$6brr1OdCq8xy$vLTySRbIBgm2KKRFCf1z1qpohpDkdo0eHfJ1dK7LXB4=',0,1,0,'2013-06-01 11:20:53','2013-06-01 11:20:33');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_fbfc09f1` (`user_id`),
  KEY `auth_user_groups_bda51c3c` (`group_id`),
  CONSTRAINT `group_id_refs_id_f0ee9890` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `user_id_refs_id_831107f1` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_fbfc09f1` (`user_id`),
  KEY `auth_user_user_permissions_1e014c8f` (`permission_id`),
  CONSTRAINT `permission_id_refs_id_67e79cb` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `user_id_refs_id_f2045483` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_fbfc09f1` (`user_id`),
  KEY `django_admin_log_e4470c6e` (`content_type_id`),
  CONSTRAINT `content_type_id_refs_id_288599e6` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `user_id_refs_id_c8665aa` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2013-04-09 17:04:41',1,7,'1','GameModel object',1,''),(2,'2013-04-09 19:43:15',1,9,'1','SubmissionModel object',1,''),(3,'2013-04-09 19:43:23',1,9,'2','SubmissionModel object',1,'');
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'permission','auth','permission'),(2,'group','auth','group'),(3,'user','auth','user'),(4,'content type','contenttypes','contenttype'),(5,'session','sessions','session'),(6,'site','sites','site'),(7,'game model','games','gamemodel'),(8,'log entry','admin','logentry'),(9,'submission model','games','submissionmodel'),(10,'round model','games','roundmodel'),(11,'migration history','south','migrationhistory'),(12,'game version model','games','gameversionmodel'),(13,'rating history model','games','ratinghistorymodel'),(14,'match history model','games','matchhistorymodel'),(15,'game options model','games','gameoptionsmodel');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_c25c2c28` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('0ddf5e6de349edd795ec18400104df6c','ZmMwNzY2YzM3MzE1Yzg1MmUwMDZkOTkxYjYwNWQxNGZmMWI1MzFiNDqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQd1Lg==\n','2013-06-15 11:20:53'),('2a31a6baba851173b4e108fc2774fb1e','NTcyNzMxZWEzOGY1MjI1ZDhhNTIxNmUwODM2N2Q3ZWIwYjQ0Yzk0YzqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==\n','2013-04-23 16:52:58'),('2be2dd2768b8c38f7a1cf9557377dcb1','NTcyNzMxZWEzOGY1MjI1ZDhhNTIxNmUwODM2N2Q3ZWIwYjQ0Yzk0YzqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==\n','2013-07-01 01:40:07'),('3620d59a95ea0a235b75e7335c69d5e3','N2RjNWU1N2NkNjI0NGIyZjcyMDQ5ZDkwOTczYjZjNWFkYzUxYTVhNDqAAn1xAS4=\n','2013-07-03 18:48:20'),('868712bcb4ae10fa38b0afca4d3c31d7','NTcyNzMxZWEzOGY1MjI1ZDhhNTIxNmUwODM2N2Q3ZWIwYjQ0Yzk0YzqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==\n','2013-07-06 12:12:03'),('9b7ae44091ec7cadd3bd7ae09e54b9a9','NTcyNzMxZWEzOGY1MjI1ZDhhNTIxNmUwODM2N2Q3ZWIwYjQ0Yzk0YzqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==\n','2013-06-10 14:20:31'),('a6b34abd87528965316e2aaacab09a97','NTcyNzMxZWEzOGY1MjI1ZDhhNTIxNmUwODM2N2Q3ZWIwYjQ0Yzk0YzqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==\n','2013-06-25 12:46:02');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_site`
--

DROP TABLE IF EXISTS `django_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_site`
--

LOCK TABLES `django_site` WRITE;
/*!40000 ALTER TABLE `django_site` DISABLE KEYS */;
INSERT INTO `django_site` VALUES (1,'example.com','example.com');
/*!40000 ALTER TABLE `django_site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game`
--

DROP TABLE IF EXISTS `game`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(512) NOT NULL,
  `has_image` tinyint(1) NOT NULL,
  `sample_submission_id` int(11),
  `options_id` int(11),
  PRIMARY KEY (`id`),
  KEY `game_8b0f36e0` (`sample_submission_id`),
  KEY `game_8c22fbdd` (`options_id`),
  CONSTRAINT `options_id_refs_id_43d9393e4451b322` FOREIGN KEY (`options_id`) REFERENCES `game_options` (`id`),
  CONSTRAINT `sample_submission_id_refs_id_547603d7e1486233` FOREIGN KEY (`sample_submission_id`) REFERENCES `submission` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game`
--

LOCK TABLES `game` WRITE;
/*!40000 ALTER TABLE `game` DISABLE KEYS */;
INSERT INTO `game` VALUES (1,'tictactoe','A classic game of tic-tac-toe. Place X\'s or O\'s inside a 3 by 3 matrix until one player completes an entire line, column or diagonal.',1,27,1),(2,'dots','A nice game of dots. Given a matrix color a border of each cell. Every time a 1 by 1 cell is completely enclosed the player that colored the last border claims that cell for him.',1,37,2);
/*!40000 ALTER TABLE `game` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_options`
--

DROP TABLE IF EXISTS `game_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `move_time_limit` int(11) NOT NULL,
  `total_time_limit` int(11) NOT NULL,
  `memory_limit` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_options`
--

LOCK TABLES `game_options` WRITE;
/*!40000 ALTER TABLE `game_options` DISABLE KEYS */;
INSERT INTO `game_options` VALUES (1,5,1,200),(2,5,180,250);
/*!40000 ALTER TABLE `game_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_versions`
--

DROP TABLE IF EXISTS `game_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `game_id` int(11) NOT NULL,
  `statement` longtext NOT NULL,
  `created_at` date NOT NULL,
  `created_by_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `game_versions_7b333d1e` (`game_id`),
  KEY `game_versions_b5de30be` (`created_by_id`),
  CONSTRAINT `created_by_id_refs_id_20de4af5068d29f1` FOREIGN KEY (`created_by_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `game_id_refs_id_10cb5aaff3a8ee52` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_versions`
--

LOCK TABLES `game_versions` WRITE;
/*!40000 ALTER TABLE `game_versions` DISABLE KEYS */;
INSERT INTO `game_versions` VALUES (1,1,'This is a game of tictactoe. You will be given a 3 by 3 matrix which is initially empty and players are assigned a symbol (X or O) and place them alternatively in the matrix. Once a player makes a complete line, column or diagonal that player wins.\r\n','2013-06-10',1),(2,1,'This is a game of tictactoe. You will be given a 3 by 3 matrix which is initially empty and players are assigned a symbol (X or O) and place them alternatively in the matrix.</textarea> Once a player makes a complete line, column or diagonal that player wins.\r\n','2013-06-10',1),(3,1,'This is a game of *tictactoe*. You will be given a 3 by 3 matrix which is initially empty and players are assigned a symbol (X or O) and place them alternatively in the matrix. Once a player makes a complete line, column or diagonal that player wins.\r\n','2013-06-10',1);
/*!40000 ALTER TABLE `game_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `match_history`
--

DROP TABLE IF EXISTS `match_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `match_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `winner_id` int(11),
  `round_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `match_history_a703c31d` (`winner_id`),
  KEY `match_history_70086e75` (`round_id`),
  CONSTRAINT `round_id_refs_id_49cae2292558b909` FOREIGN KEY (`round_id`) REFERENCES `round` (`id`),
  CONSTRAINT `winner_id_refs_id_5fd44fdcaf8e044a` FOREIGN KEY (`winner_id`) REFERENCES `submission` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=453 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `match_history`
--

LOCK TABLES `match_history` WRITE;
/*!40000 ALTER TABLE `match_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `match_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `match_history_submissions`
--

DROP TABLE IF EXISTS `match_history_submissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `match_history_submissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `matchhistorymodel_id` int(11) NOT NULL,
  `submissionmodel_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `match_history_submis_matchhistorymodel_id_5040814cede8ec5c_uniq` (`matchhistorymodel_id`,`submissionmodel_id`),
  KEY `match_history_submissions_405e8379` (`matchhistorymodel_id`),
  KEY `match_history_submissions_81a2cfc0` (`submissionmodel_id`),
  CONSTRAINT `matchhistorymodel_id_refs_id_428438cec813a8eb` FOREIGN KEY (`matchhistorymodel_id`) REFERENCES `match_history` (`id`),
  CONSTRAINT `submissionmodel_id_refs_id_47ee82b45427a4bc` FOREIGN KEY (`submissionmodel_id`) REFERENCES `submission` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=905 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `match_history_submissions`
--

LOCK TABLES `match_history_submissions` WRITE;
/*!40000 ALTER TABLE `match_history_submissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `match_history_submissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating_history`
--

DROP TABLE IF EXISTS `rating_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rating_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` int(11) NOT NULL,
  `submission_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rating_history_b3d6235a` (`submission_id`),
  CONSTRAINT `submission_id_refs_id_220fb0baff497cd3` FOREIGN KEY (`submission_id`) REFERENCES `submission` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=376 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rating_history`
--

LOCK TABLES `rating_history` WRITE;
/*!40000 ALTER TABLE `rating_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `rating_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `round`
--

DROP TABLE IF EXISTS `round`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `round` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `index` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `results` longtext NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `round_7b333d1e` (`game_id`),
  CONSTRAINT `game_id_refs_id_17aed63c` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `round`
--

LOCK TABLES `round` WRITE;
/*!40000 ALTER TABLE `round` DISABLE KEYS */;
/*!40000 ALTER TABLE `round` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `round_submissions`
--

DROP TABLE IF EXISTS `round_submissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `round_submissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roundmodel_id` int(11) NOT NULL,
  `submissionmodel_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roundmodel_id` (`roundmodel_id`,`submissionmodel_id`),
  KEY `round_submissions_7c3b4a27` (`roundmodel_id`),
  KEY `round_submissions_81a2cfc0` (`submissionmodel_id`),
  CONSTRAINT `roundmodel_id_refs_id_2dc6bd0f` FOREIGN KEY (`roundmodel_id`) REFERENCES `round` (`id`),
  CONSTRAINT `submissionmodel_id_refs_id_c86f4b9e` FOREIGN KEY (`submissionmodel_id`) REFERENCES `submission` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=190 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `round_submissions`
--

LOCK TABLES `round_submissions` WRITE;
/*!40000 ALTER TABLE `round_submissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `round_submissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `south_migrationhistory`
--

DROP TABLE IF EXISTS `south_migrationhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `south_migrationhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_name` varchar(255) NOT NULL,
  `migration` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `south_migrationhistory`
--

LOCK TABLES `south_migrationhistory` WRITE;
/*!40000 ALTER TABLE `south_migrationhistory` DISABLE KEYS */;
INSERT INTO `south_migrationhistory` VALUES (1,'games','0001_initial','2013-04-29 14:48:30'),(2,'games','0002_auto__add_field_gamemodel_description__add_field_gamemodel_has_image','2013-04-29 14:49:35'),(3,'games','0003_auto__add_field_gamemodel_statement','2013-05-10 16:20:01'),(4,'games','0004_auto__add_field_submissionmodel_rating','2013-05-25 12:24:41'),(5,'games','0005_auto__add_field_submissionmodel_active','2013-05-27 02:39:45'),(6,'games','0006_auto__add_field_submissionmodel_created_at','2013-05-29 22:23:08'),(7,'games','0007_auto__add_field_submissionmodel_name','2013-05-30 20:03:47'),(8,'games','0008_auto__del_field_submissionmodel_active__add_field_submissionmodel_stat','2013-06-08 15:08:05'),(9,'games','0009_auto__add_field_gamemodel_sample_submission','2013-06-08 20:09:47'),(10,'games','0010_auto__add_field_submissionmodel_test_submission','2013-06-09 04:16:12'),(11,'games','0011_auto__add_gameversionmodel__del_field_gamemodel_statement','2013-06-10 21:26:59'),(12,'games','0012_auto__add_ratinghistorymodel__chg_field_submissionmodel_rating','2013-06-14 00:07:06'),(13,'games','0013_auto__del_field_submissionmodel_rating','2013-06-14 00:10:54'),(14,'games','0014_auto__add_field_submissionmodel_rating','2013-06-14 01:08:00'),(15,'games','0015_auto__add_field_roundmodel_date','2013-06-16 01:01:55'),(16,'games','0016_auto__chg_field_ratinghistorymodel_date','2013-06-16 18:01:27'),(17,'games','0017_auto__add_matchhistorymodel','2013-06-16 19:55:35'),(18,'games','0018_auto__chg_field_matchhistorymodel_round','2013-06-16 20:07:14'),(19,'games','0019_auto__chg_field_matchhistorymodel_winner__chg_field_matchhistorymodel_','2013-06-16 20:13:43'),(20,'games','0020_auto__add_gameoptionsmodel__add_field_gamemodel_options','2013-06-20 15:49:40');
/*!40000 ALTER TABLE `south_migrationhistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `submission`
--

DROP TABLE IF EXISTS `submission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `submission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `name` varchar(256) NOT NULL,
  `status` varchar(32) NOT NULL,
  `test_submission` tinyint(1) NOT NULL,
  `rating` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `submission_fbfc09f1` (`user_id`),
  KEY `submission_7b333d1e` (`game_id`),
  CONSTRAINT `game_id_refs_id_fed11163` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`),
  CONSTRAINT `user_id_refs_id_6270ce8e` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `submission`
--

LOCK TABLES `submission` WRITE;
/*!40000 ALTER TABLE `submission` DISABLE KEYS */;
INSERT INTO `submission` VALUES (27,1,1,'2013-05-30','Random #1','active',0,1270),(28,1,1,'2013-05-30','Random #2','active',0,1339),(29,1,1,'2013-05-30','Random #3','active',0,1154),(30,1,1,'2013-05-30','Random #4','active',0,1163),(31,1,1,'2013-05-30','Random #5','active',0,1068),(32,1,1,'2013-05-30','First-free #1','active',0,1499),(33,1,1,'2013-05-30','First-free #2','active',0,1377),(34,1,1,'2013-05-30','First-free #3','active',0,1171),(35,1,1,'2013-05-30','First-free #4','active',0,1294),(36,1,1,'2013-05-30','First-free #5','active',0,1156),(37,1,2,'2013-05-30','Sample-ai #1','active',0,1200),(38,1,2,'2013-05-30','Sample-ai #2','active',0,1200),(39,1,1,'2013-06-08','Random #6','active',0,1040),(40,1,1,'2013-06-08','First-free #6','active',0,1137),(41,1,1,'2013-06-08','Test-submission-cron','active',0,1340),(42,1,1,'2013-06-08','Test-cron #2','active',0,1255),(43,1,1,'2013-06-08','Test-cron #3','active',0,1149),(44,1,1,'2013-06-08','Test-cron #4','active',0,1050),(45,1,1,'2013-06-08','Test-cron #5','active',0,1037),(46,1,1,'2013-06-08','test-cron #6','active',0,1135),(47,1,1,'2013-06-08','test-cron #7','active',0,1273),(55,1,1,'2013-06-17','test-cron #8','inactive',0,1200),(56,1,1,'2013-06-17','test-cron #9','inactive',0,1200),(57,1,1,'2013-06-17','test-cron #10','inactive',0,1200),(58,1,1,'2013-06-17','test-cron #11','inactive',0,1200),(59,1,1,'2013-06-17','test-cron #12','inactive',0,1200),(60,1,1,'2013-06-17','test-cron #13','inactive',0,1200),(61,1,1,'2013-06-17','test-cron #14','inactive',0,1200),(62,1,1,'2013-06-17','test-cron #15','inactive',0,1200),(63,1,1,'2013-06-17','test-cron #16','inactive',0,1200);
/*!40000 ALTER TABLE `submission` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-06-22 15:08:15
