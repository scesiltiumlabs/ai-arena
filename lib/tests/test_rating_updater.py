from unittest2 import TestCase
from lib.rating_calculator import RatingCalculator

# Fixture
player_ratings = {
   1: 1613,
   2: 1609,
   3: 1477,
   4: 1388,
   5: 1586,
   6: 1720
}

matches = [
   [1, 2, 1],
   [1, 3, 0.5],
   [1, 4, 0],
   [1, 5, 0],
   [1, 6, 1]
]

# Player1 expected final rating
EXPECTED_RATING = 1601

# Player1 expected real score
EXPECTED_REAL_SCORE = 2.5

# Player1 expected estimated score
EXPECTED_ESTIMATED_SCORE = 2.867


class TestRatingUpdater(TestCase):
    def setUp(self):
        self.rating_calculator = RatingCalculator()

    def test_expected_scores(self):
        """ Test the get_expected_scores method from the rating calculator. """
        expected_scores = self.rating_calculator._get_expected_scores(
                            player_ratings, matches)
        self.assertAlmostEqual(expected_scores[1], EXPECTED_ESTIMATED_SCORE, 2)

    def test_real_score(self):
        """ Test the get_real_scores method from the rating calculcator. """
        real_scores = self.rating_calculator._get_real_scores(matches)
        self.assertEqual(real_scores[1], EXPECTED_REAL_SCORE)

    def test_final_rating(self):
        """ Test the final rating of the player with id number 1 is the one
        we expect.
        """
        new_ratings = self.rating_calculator.compute_new_ratings(player_ratings,
                                                                 matches)
        self.assertAlmostEqual(new_ratings[1], EXPECTED_RATING, -1)
