import simplejson as json
import time

from .redis_client import RedisClient

QUEUE_SLEEP_INTERVAL = 0.1

class QueueTimeoutException(Exception):
    pass

class Queue(object):
    """ TODO: write comment """

    def __init__(self, name):
        self.name = name
        self.redis_client = RedisClient()

    def pop(self, blocking=True, timeout=0):
        started = time.time()
        while True:
            value = self.redis_client.server.lpop(self.name)
            if value:
                return json.loads(value)

            if not blocking:
                return None

            if timeout and time.time() - started > timeout:
                raise QueueTimeoutException('No message arrived on the queue')

            time.sleep(QUEUE_SLEEP_INTERVAL)

    def push(self, value):
        self.redis_client.server.rpush(self.name, json.dumps(value))

    def flush(self):
        while self.pop(blocking=False):
            continue
