from django.conf import settings
 
from sandbox import Sandbox, SandboxConfig
from sandbox import Timeout

class TimeoutException(Exception):
    pass

class MemoryLimitExceeded(Exception):
    pass

class Jail(object):
    def __init__(self):
        self.sandbox_config = SandboxConfig('stdout', 'import')
        self.sandbox_config._set_max_memory(256)
        self.sandbox_config._set_timeout(5)
        self.sandbox = Sandbox(self.sandbox_config)

    def hide_settings(self):
        self.old_settings = {}
        for setting in dir(settings):
            if not setting.startswith('__'):
                self.old_settings[setting] = getattr(settings, setting, None)
                setattr(settings, setting, None)

    def restore_settings(self):
        for setting in self.old_settings:
            if not setting.startswith('__'):
                setattr(settings, setting, self.old_settings[setting])

    def execute(self, function, *args, **kwargs):
        self.hide_settings()
        try:
            result = self.sandbox.call(function, *args, **kwargs)
        except Timeout:
            # We're raising a different exception for consistency (upper levels
            # don't have knowledge of the sandbox module and it's exceptions)
            raise TimeoutException('Time limit exceeded')

        self.restore_settings()
        return result
