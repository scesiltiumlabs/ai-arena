import simplejson as json

from django.conf import settings
from redis import StrictRedis

class RedisClient(object):

    def __init__(self):
        self.server = StrictRedis(host=settings.REDIS_HOST,
                                   port=settings.REDIS_PORT)

    def set(self, key, value):
        self.server.set(key, json.dumps(value))

    def get(self, key):
        return json.loads(self.server.get(key))
