from collections import defaultdict

K_FACTOR_MASTERS = 16
K_FACTOR_NON_MASTERS = 32
MASTERS_RATING_CUTOFF = 2100

class PlayerRatingNotFound(Exception):
    pass

class RatingCalculator(object):
    def __init__(self):
        pass

    def get_player_score(self, player, match):
        """ Given a player_id and a match in which he played, this method
        will return the score of the player.
            * 1 if the player won
            * 0.5 if it was a tie
            * 0 it the player lost
        """
        if match[1] == 'TIE':
            return 0.5
        elif match[1] == player:
            return 1
        else:
            return 0

    def _get_expected_scores(self, player_ratings, matches):
        """ Given the player ratings and the list of played matches
        returns a dict with the estimated number of points each player
        should get according to their previous ratings.
        """
        expected_scores = defaultdict(int)
        for match in matches:
            player1_id = match[0][0]
            player2_id = match[0][1]
            player1_rating = player_ratings.get(player1_id)
            player2_rating = player_ratings.get(player2_id)

            if not player1_rating:
                print player1_id
                raise PlayerRatingNotFound(
                    "Couldn't find player rating for player %s" % player1_id)

            if not player2_rating:
                raise PlayerRatingNotFound(
                      "Couldn't find player rating for player %s" % player2_id)

            Q1 = 10 ** (1.0 * player1_rating / 400)
            Q2 = 10 ** (1.0 * player2_rating / 400)

            player1_estimate = Q1 / (Q1 + Q2)
            player2_estimate = Q2 / (Q1 + Q2)

            expected_scores[player1_id] += player1_estimate
            expected_scores[player2_id] += player2_estimate

        return expected_scores

    def _get_real_scores(self, matches):
        """ Receives a list of matches and returns the number of points
        each player got.
        """
        real_scores = defaultdict(int)
        for match in matches:
            player1_id = match[0]
            player2_id = match[1]
            real_scores[player1_id] += self.get_player_score(player1_id, match)
            real_scores[player2_id] += self.get_player_score(player2_id, match)

        return real_scores

    def get_kfactor(self, player_rating):
        if player_rating > MASTERS_RATING_CUTOFF:
            return K_FACTOR_MASTERS
        else:
            return K_FACTOR_NON_MASTERS

    def compute_new_ratings(self, player_ratings, matches):
        """ This method updates the ratings of all the players involved
        in a group given the results of the group.
        The players argument represents a list of submissions representing the
        players which were part of this group.
        The matches argument is a list of matches represented in the following
        format:
        [player1 id, player2 id, score]
        Score can be 0 if player1 won, 1 if player2 won or 0.5 if the game was 
        a tie.
        """
        new_ratings = {}
        expected_scores = self._get_expected_scores(player_ratings, matches)
        real_scores = self._get_real_scores(matches)

        for player_id, player_rating in player_ratings.iteritems():
            real_score = real_scores[player_id]
            expected_score = expected_scores[player_id]

            K = self.get_kfactor(player_rating)
            new_ratings[player_id] = (player_rating + 
                                      K * (real_score - expected_score))

        return new_ratings
